== Changelog ==

= 1.4.0 =
* Added shortcodes for all attributes available.

= 1.3.9 =
* Fixed get_years shortcode to no longer use the deprecated starting year.

= 1.3.8 =
* Fixed issue with update 1.3.7 not filling out the fields correctly.

= 1.3.7 =
* Minor updates to the SBG Site Config to allow for removal of field items.

= 1.3.6 =
* Social Media shortcode link tags will now use target='_blank'.
* Increased maximum size of locality footer text.
* Fixed issues with the Site Config page where broken image tags were showing.
* Temporarily hiding Divi Helper section until implementation is complete.

= 1.3.5 =
* Updated Social Media shortcode to also account for if the user would like square icons instead of the standard icons.

= 1.3.4 =
* Added Social Media Shortcode

= 1.3.3 =
* Updated Legacy Attributes to write to old attributes when chosen to keep the old attributes.

= 1.3.2 =
* Updated plugin to keep both the new and legacy database fields. This will support backwards compatibility with the SBG Utils.

= 1.3.1 =
* Fixed image url scheme on sites without https.

= 1.3.0 =
* Added Divi image alt text fix.
* Legacy functions are now included on every page.
* Added legacy sbg_get_years() function.

= 1.2.0 =
* Testing ability to auto update via WebHook through Pull Requests.

= 1.1.1 =
* Minor Bump in version to catch up with tags.

= 0.5.0 =
* Initial Deploy