<?php
/**
 * Plugin Name: Compulse - Site Configuration
 * Plugin URI: http://compulse.com
 * Description: Easy access site configuration for both SBP, Custom and Landing Pages.
 * Version: 1.4.2
 * Author: Andy Nguyen and Kevin Hall, Compulse
 * Author URI: https://compulse.com
 * Bitbucket Plugin URI: https://bitbucket.org/sinclairdigitalsolutions/sbg-site-config
 */


include 'lib/site-config-shortcodes.php';
include 'include/divi-image-alt-text-fix.php';
include 'lib/site-config-legacy.php';

function register_global_stylesheets () {
	wp_register_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	wp_enqueue_style('font-awesome');
}
add_action('wp_enqueue_scripts', 'register_global_stylesheets');

function register_site_config_settings () {
	$keys = [
		'site-name', 'company-name', 'company-address', 'company-city', 'company-state', 'company-zip',
		'company-email', 'company-phone', 'company-phone-alt', 'company-fax', 'company-locality',
		'facebook', 'twitter', 'tumblr', 'google', 'instagram', 'pinterest', 'linkedin', 'youtube',
		'facebook-url', 'twitter-url', 'tumblr-url', 'google-url', 'instagram-url', 'pinterest-url', 'linkedin-url',
		'youtube-url', 'site-logo', 'site-favicon', 'site-footer-logo'
	];

	foreach ($keys as $key) {
		register('sct-' . $key);
	}
}
add_action('admin_init', 'register_site_config_settings');

function register ($key) {
	register_setting('sbp-site-config', $key);
}

function custom_site_configuration () {
	$page_title = 'Site Configuration';
	$menu_title = 'Site Config';
	$capability = 'edit_posts';
	$menu_slug  = 'site_config';
	$icon_url   = 'dashicons-laptop';
	$position   = 59;
	$callback   = 'site_config_display';

	add_menu_page($page_title, $menu_title, $capability, $menu_slug, $callback, $icon_url, $position);
}
add_action('admin_menu', 'custom_site_configuration');

function site_config_display () {
	// Include Font Awesome
	$user = wp_get_current_user();
	if ($user->roles[0] == 'administrator') {
		wp_enqueue_media();
		wp_enqueue_script('jquery');
		wp_enqueue_script('thickbox');
		wp_enqueue_style('thickbox');
		wp_enqueue_script('media-upload');

		include 'lib/site-config-header.php';
		echo '<div style="display: flex; flex-direction: row; padding: 25px 50px;">';
		echo '<form class="core-config-form" method="post" action="options.php" style="display: flex; flex-direction: row; width: 100%; flex-flow: row wrap;">';
		include 'lib/site-config-form.php';
		include 'lib/site-config-socialmedia.php';
		include 'lib/site-config-images.php';
		echo '</form>';
		echo '</div>';
		include 'lib/site-config-divi.php';

	} else {
		echo '<h1>You do not have permission to edit this.</h1>';
		echo '<p>Please contact an administrator if you need to make changes to the site information.</p>';
	}
}

// Helper Function for migrating old plugin information.
function checkLegacyAttribute ($oldKey, $newKey, &$attribute, $keepOld = false) {
	$oldAttribute = get_option($oldKey);
	$newAttribute = get_option($newKey);

	if ($oldAttribute && $newAttribute) {
		// If they both exist, remove the old one and return only the new attribute.

		if ($keepOld) {
			$attribute = $newAttribute;
			update_option($oldKey, $newAttribute);

		} else {
			$attribute = $newAttribute;
			delete_option($oldKey);
		}

	} elseif (!$oldAttribute && !$newAttribute) {
		// If neither exist, do nothing.

	} elseif ($oldAttribute && !$newAttribute) {
		// If the old attribute exists, but the new one is empty, fill the new option and return the old value.
		$attribute = '';
		update_option($newKey, '');

		if ($keepOld) {
			update_option($oldKey, $attribute);
		} else {
			delete_option($oldKey);
		}

	} else {
		// Default to new key only.
		$attribute = $newAttribute;

		if ($keepOld) {
			update_option($oldKey, $newAttribute);
		}
	}
}
