<?php

/**
*   Legacy Shortcodes from SBG Utils Plugin
*/

// SBG class
class SBG {
	private $coName = 'Compulse';
	private $coURL = 'https://compulse.com';
	private $coSlogan = 'Solutions at your fingertips';

	public function getCompanyName() {
		return 	$this->coName;
	}

	public function printComapanyName() {
		echo $this->coName;
	}

	public function printCompanyName() {
		echo $this->coName;
	}

	public function getCompanyURL() {
		return $this->coURL;
	}

	public function printCompanyURL() {
		echo $this->coURL;
	}

	public function getCompanySlogan() {
		return $this->coSlogan;
	}

	public function printCompanySlogan() {
		echo $this->coSlogan;
	}
}
$GLOBALS['sbg'] = new SBG();

//// SHORT CODES ////
// add shortcode method to retrieve sbp options
function shortcode_sbp_config($atts) {
	$setting = shortcode_atts( array(
		'setting' => FALSE,
	), $atts );

	$setting = $setting['setting'];

	if ($setting) {
		ob_start();
		try {
			switch ($setting) {
				case 'phone':
					echo et_get_option( 'phone_number' );
					break;
				case 'email':
					echo et_get_option( 'header_email' );
					break;
				default:
					echo get_option('sbp-' . $setting);
					break;
			}
		} catch (Exception $e) {
			// do nothing this is a failsafe just in case Divi decides to change up their code
		}

		return ob_get_clean();
	}

	return null;
}
add_shortcode( 'sbp_config', 'shortcode_sbp_config' );

// quick shortcode for address block
function shortcode_sbp_address_block() {
	$getDirections = 'https://maps.google.com?daddr=' . get_option('sct-company-address') .'+'. get_option('sct-company-city') .'+'. get_option('sct-company-state') .'+'. get_option('sct-company-zip');
	ob_start();
	?>
	<address class="sbp-address-block">
		<div class="sbp-company-name"><?php echo get_option('sbp-company-name') ?></div>
		<div class="sbp-address-line1"><a href="<?php echo $getDirections ?>"><?php echo get_option('sct-company-address') ?></a></div>
		<div class="sbp-address-line2"><a href="<?php echo $getDirections ?>"><?php echo get_option('sct-company-city') ?>, <?php echo get_option('sct-company-state') ?> <?php echo get_option('sct-company-zip') ?></a></div>
	</address>
	<?php
	return ob_get_clean();
}
add_shortcode( 'sbp_address_block', 'shortcode_sbp_address_block' );

// code for getting address in an anchor that links to google to get directions
function shortcode_sbp_get_directions() {
	$getDirections = 'https://maps.google.com?daddr=' . get_option('sct-company-address') .'+'. get_option('sct-company-city') .'+'. get_option('sct-company-state') .'+'. get_option('sct-company-zip');
	ob_start(); ?>
	<a class="sbp-get-dir" href="<?php echo $getDirections ?>"><?php echo get_option('sct-company-address') ?><br><?php echo get_option('sct-company-city') ?>, <?php echo get_option('sct-company-state') ?> <?php echo get_option('sct-company-zip') ?></a>
	<?php return ob_get_clean();
}
add_shortcode( 'sbp_getdir', 'shortcode_sbp_get_directions' );


// shortcode for getting all comany info in a nice list
function shortcode_sbp_company_info() {
	$info = '';
	$name = get_option('sct-company-name');
	$address = get_option('sct-company-address');
	$city = get_option('sct-company-city');
	$state = get_option('sct-company-state');
	$zip = get_option('sct-company-zip');
	$email = get_option('sct-company-email');
	$phone = get_option('sct-company-phone');
	$phone2 = get_option('sct-company-phone-alt');
	$fax = get_option('sct-company-fax');

	try {
		if ( empty($email) )
			$email = et_get_option( 'header_email' );

		if ( empty($phone) )
			$phone = et_get_option( 'phone_number' );
	} catch (Exception $e) {}


	if ($name) {
		$info .= "<strong>$name</strong><br />";
	}

	if ($address && $city && $state && $zip) {
		$info .= "<strong>Address:</strong><br />$address<br />$city, $state $zip<br />";
	}

	if ($email) {
		$info .= "<strong>Email:</strong><br /><a href=\"mailto:$email\">$email</a><br />";
	}

	if ($phone) {
		$info .= "<strong>Phone:</strong><br />$phone<br />";
	}

	if ($phone2) {
		$info .= "<strong>Phone2:</strong><br />$phone2<br />";
	}

	if ($fax) {
		$info .= "<strong>Fax:</strong><br />$fax<br />";
	}

	ob_start();
	echo $info;
	return ob_get_clean();
}
add_shortcode( 'sbp_company_info', 'shortcode_sbp_company_info' );

// Shortcode for grabbing the page name through DIVI.
function shortcode_sbp_page_title () {
	ob_start();
	echo get_the_title();
	return ob_get_clean();
}
add_shortcode( 'sbp_title', 'shortcode_sbp_page_title' );

// util function to get year span from start
function sbg_get_years() {
	$curYear = date('Y');
	echo ($curYear);
}
