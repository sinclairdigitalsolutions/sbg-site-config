<?php
	$social_media = [
		['name' => 'Facebook', 'icon' => 'fa-facebook-square', 'class' => 'scs-facebook', 'namespace' => 'facebook'],
		['name' => 'Twitter', 'icon' => 'fa-twitter-square', 'class' => 'scs-twitter', 'namespace' => 'twitter'],
		['name' => 'Tumblr', 'icon' => 'fa-tumblr-square', 'class' => 'scs-tumblr', 'namespace' => 'tumblr'],
		['name' => 'Google+', 'icon' => 'fa-google-plus-square', 'class' => 'scs-google-plus', 'namespace' => 'google'],
		['name' => 'Instagram', 'icon' => 'fa-instagram', 'class' => 'scs-instagram', 'namespace' => 'instagram'],
		['name' => 'Pinterest', 'icon' => 'fa-pinterest-square', 'class' => 'scs-pinterest', 'namespace' => 'pinterest'],
		['name' => 'LinkedIn', 'icon' => 'fa-linkedin-square', 'class' => 'scs-linkedin', 'namespace' => 'linkedin'],
		['name' => 'YouTube', 'icon' => 'fa-youtube-square', 'class' => 'scs-youtube', 'namespace' => 'youtube']
	];
?>

<div style="margin-right: 50px; min-width: 500px;">
	<h2 style="text-align: center">Social Media Settings</h2>
    <p style="text-align: left"><small>Note: Social Media urls cannot be changed unless the element is enabled.</small></p>
        <ul>
            <?php foreach ($social_media as $platform): ?>
            <li class="site-config-social-node">
                <span class="site-config-social-title"><?php echo $platform['name']; ?></span>

                <div class="site-config-node-row">
    <!--				<i class="fa --><?php //echo $platform['icon']; ?><!-- --><?php //echo $platform['class']; ?><!--" style="margin-right: 15px; --><?php //if ($platform['name'] == 'Instagram') { echo 'font-size: 28px'; } else { echo 'font-size: 40px'; } ?><!--"></i>-->
                    <span class="site-config-node-row-block">Enable:</span>
                    <label class="sbp-switch site-config-node-row-block">
                        <input type="checkbox" id="sct-<?php echo $platform['namespace']; ?>" name="sct-<?php echo $platform['namespace']; ?>" <?php if (esc_attr(get_option('sct-' . $platform['namespace'])) == 'on') { echo 'checked'; } ?>/>
                        <span class="sbp-slider sbp-round"></span>
                    </label>
                    <span class="site-config-node-row-block">Url:</span>
                    <input
                            id="sct-<?php echo $platform['namespace']; ?>-field"
                            type="text" <?php if (esc_attr(get_option('sct-' . $platform['namespace'])) != 'on') { echo 'disabled'; }?>
                            value="<?php echo get_option('sct-' . $platform['namespace'] . '-url'); ?>"
                            name="sct-<?php echo $platform['namespace']; ?>-url"
                    />
                </div>
            </li>

            <script>
                document.querySelector('input[name=sct-<?php echo $platform['namespace']; ?>').addEventListener('change', function () {
                    if (this.checked) {
                        document.getElementById('sct-<?php echo $platform['namespace']; ?>-field').disabled = false;
                    } else {
                        document.getElementById('sct-<?php echo $platform['namespace']; ?>-field').disabled = true;
                    }
                });
            </script>
            <?php endforeach; ?>
        </ul>
</div>

<style>
	.sbp-switch {
		position: relative;
		display: inline-block;
		width: 30px;
		height: 12px;
	}

	.sbp-switch input {display: none;}

	.sbp-slider {
		position: absolute;
		cursor: pointer;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: #ccc;
		transition: 400ms;
		padding: 5px 0;
	}

	.sbp-slider:before {
		position: absolute;
		content: "";
		height: 10px;
		width: 10px;
		left: 1px;
		bottom: 1px;
		background-color: white;
		-webkit-transition: .4s;
		transition: .4s;
	}

	.sbp-switch input:checked + .sbp-slider {
		background-color: rgb(248, 76, 0);
	}

	.sbp-switch input:focus + .sbp-slider {
		box-shadow: 0 0 1px rgb(248, 76, 0);
	}

	.sbp-switch input:checked + .sbp-slider:before {
		-webkit-transform: translateX(18px);
		-ms-transform: translateX(18px);
		transform: translateX(18px);
	}

	.sbp-slider.sbp-round {
		border-radius: 11px;
	}

	.sbp-slider.sbp-round:before {
		border-radius: 50%;
	}

	.site-config-social-title {
		font-size: 14px;
		font-weight: 600;
		letter-spacing: 1px;
        margin-right: 15px;
	}

	.site-config-social-node {
		margin-bottom: 15px;
        text-align: left;
	}

	.scs-facebook {  color: #3b5998; }
	.scs-twiiter {  color: #1da1f2; }
	.scs-google-plus {  color: #dd4b39; }
	.scs-linkedin { color: #0077b5; }
	.scs-pinterest { color: #bd081c; }
	.scs-tumblr { color: #35465c; }
	.scs-instagram {
		color: white;
		padding: 5px;
		border-radius: 5px;
		background: linear-gradient(-45deg, #405de6, #5851db, #883ab4, #c13584, #e1306c, #fd1d1d, #f56040, #f77737, #fcaf45, #ffdc80);
	}
	.scs-youtube { color: #ff0000; }

	.site-config-node-row {
		display: flex;
		flex-direction: row;
		align-items: center;
		justify-content: space-between;
	}

    .site-config-node-row-block {
        margin-right: 10px;
    }
</style>