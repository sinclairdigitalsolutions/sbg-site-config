<?php
    $upload_link = esc_url(get_upload_iframe_src('image', null, 'library'));

    function sct_custom_image_tabs ($_default_tabs) {
        unset($_default_tabs['type']);
        unset($_default_tabs['type_url']);
        unset($_default_tabs['gallery']);

        return ($_default_tabs);
    }
    add_filter('media_upload_tabs', 'sct_custom_image_tabs', 10, 1);

?>

<div class="sct-logo-container">
	<h2 style="text-align: center">Logo and FavIcon</h2>
    <p class="sct-logo-title">Site Logo</p>
    <p id="sct-logo-select" class="button thickbox">Media Library</p>
    <input type="text" name="sct-site-logo" id="sct-site-logo-field" value="<?php echo get_option('sct-site-logo'); ?>" />
    <?php if (get_option('sct-site-logo')) : ?>
        <img src="<?php echo get_option('sct-site-logo'); ?>" id="sct-preview-logo" width="200px" height="auto"/>
    <?php endif; ?>

    <p class="sct-logo-title">Site Footer Logo</p>
    <p id="sct-footer-select" class="button thickbox">Media Library</p>
    <input type="text" name="sct-site-footer-logo" id="sct-site-footer-field" value="<?php echo get_option('sct-site-footer-logo'); ?>" />
	<?php if (get_option('sct-site-footer-logo')) : ?>
        <img src="<?php echo get_option('sct-site-footer-logo'); ?>" id="sct-preview-footer-logo" width="200px" height="auto" />
    <?php endif; ?>

    <p class="sct-logo-title">Site Favicon</p>
    <p id="sct-icon-select" class="button thickbox">Media Library</p>
    <input type="text" name="sct-site-favicon" id="sct-site-icon-field" value="<?php echo get_option('sct-site-favicon'); ?>" />
	<?php if (get_option('sct-site-favicon')) : ?>
        <img src="<?php echo get_option('sct-site-favicon'); ?>" id="sct-preview-favicon" width="200px" height="auto" />
    <?php endif; ?>
</div>

<style>
    .sct-logo-container {
        width: 30%;
        text-align: center;
    }

    .sct-logo-title {
        text-align: left !important;
        font-weight: 700;
        padding-left: 35px;
        letter-spacing: 1px;
    }
</style>

<script>
    jQuery(document).ready(function(e) {
        jQuery('#sct-logo-select').on('click', function (e) {
            e.preventDefault();
            var logo_upload = wp.media({
                title: 'Select Site Logo',
                button: { text: 'Select Logo' },
                multiple: false
            });

            logo_upload.on('select', function () {
                var imageObj = logo_upload.state().get('selection').first().toJSON();
                var imageSrc = imageObj.url;
                jQuery('#sct-site-logo-field').attr({'value': imageSrc});
                jQuery('#sct-preview-logo').attr({'src': imageSrc});
            });

            logo_upload.open();
        });

        jQuery('#sct-icon-select').on('click', function (e) {
            e.preventDefault();
            var logo_upload = wp.media({
                title: 'Select Site Favicon',
                button: { text: 'Select Icon' },
                multiple: false
            });

            logo_upload.on('select', function () {
                var imageObj = logo_upload.state().get('selection').first().toJSON();
                var imageSrc = imageObj.url;
                jQuery('#sct-site-icon-field').attr({'value': imageSrc});
                jQuery('#sct-preview-favicon').attr({'src': imageSrc});
            });

            logo_upload.open();
        });

        jQuery('#sct-footer-select').on('click', function (e) {
            e.preventDefault();
            var logo_upload = wp.media({
                title: 'Select Site Footer Logo',
                button: { text: 'Select Footer Logo' },
                multiple: false
            });

            logo_upload.on('select', function () {
                var imageObj = logo_upload.state().get('selection').first().toJSON();
                var imageSrc = imageObj.url;
                jQuery('#sct-site-footer-field').attr({'value': imageSrc});
                jQuery('#sct-preview-footer-logo').attr({'src': imageSrc});
            });

            logo_upload.open();
        });
    });
</script>