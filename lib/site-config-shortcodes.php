<?php
	function shortcode_sbp_footer_social () {
		ob_start(); ?>
			<ul>
				<?php
					$social = [
						['name' => 'Facebook', 'icon' => 'fa-facebook', 'class' => 'scs-facebook', 'namespace' => 'facebook'],
						['name' => 'Twitter', 'icon' => 'fa-twitter', 'class' => 'scs-twitter', 'namespace' => 'twitter'],
						['name' => 'Tumblr', 'icon' => 'fa-tumblr', 'class' => 'scs-tumblr', 'namespace' => 'tumblr'],
						['name' => 'Google+', 'icon' => 'fa-google-plus', 'class' => 'scs-google-plus', 'namespace' => 'google'],
						['name' => 'Instagram', 'icon' => 'fa-instagram', 'class' => 'scs-instagram', 'namespace' => 'instagram'],
						['name' => 'Pinterest', 'icon' => 'fa-pinterest', 'class' => 'scs-pinterest', 'namespace' => 'pinterest'],
						['name' => 'LinkedIn', 'icon' => 'fa-linkedin', 'class' => 'scs-linkedin', 'namespace' => 'linkedin'],
						['name' => 'YouTube', 'icon' => 'fa-youtube', 'class' => 'scs-youtube', 'namespace' => 'youtube']
					];
				?>
				<?php foreach ($social as $platform) : ?>
					<?php if (get_option('sct-' . $platform['namespace']) == 'on') : ?>
						<li>
							<a href="<?php echo get_option('sct-' . $platform['namespace'] . '-url'); ?>" target="_blank">
								<i class="fa <?php echo $platform['icon']; ?>"></i>
							</a>
						</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		<?php return ob_get_clean();
	}

	add_shortcode('sbp_footer_social', 'shortcode_sbp_footer_social');
	add_shortcode('sct_social', 'shortcode_sbp_footer_social');

	function shortcode_sbp_footer_social_square () {
		ob_start(); ?>
		<ul>
			<?php
			$social = [
				['name' => 'Facebook', 'icon' => 'fa-facebook-square', 'class' => 'scs-facebook', 'namespace' => 'facebook'],
				['name' => 'Twitter', 'icon' => 'fa-twitter-square', 'class' => 'scs-twitter', 'namespace' => 'twitter'],
				['name' => 'Tumblr', 'icon' => 'fa-tumblr-square', 'class' => 'scs-tumblr', 'namespace' => 'tumblr'],
				['name' => 'Google+', 'icon' => 'fa-google-plus-square', 'class' => 'scs-google-plus', 'namespace' => 'google'],
				['name' => 'Instagram', 'icon' => 'fa-instagram', 'class' => 'scs-instagram', 'namespace' => 'instagram'],
				['name' => 'Pinterest', 'icon' => 'fa-pinterest-square', 'class' => 'scs-pinterest', 'namespace' => 'pinterest'],
				['name' => 'LinkedIn', 'icon' => 'fa-linkedin-square', 'class' => 'scs-linkedin', 'namespace' => 'linkedin'],
				['name' => 'YouTube', 'icon' => 'fa-youtube-square', 'class' => 'scs-youtube', 'namespace' => 'youtube']
			];
			?>
			<?php foreach ($social as $platform) : ?>
				<?php if (get_option('sct-' . $platform['namespace']) == 'on') : ?>
					<li>
						<a href="<?php echo get_option('sct-' . $platform['namespace'] . '-url'); ?>" target="_blank">
							<i class="fa <?php echo $platform['icon']; ?>"></i>
						</a>
					</li>
				<?php endif; ?>
			<?php endforeach; ?>
		</ul>
		<?php return ob_get_clean();
	}

	add_shortcode('sbp_footer_social_square', 'shortcode_sbp_footer_social_square');
	add_shortcode('sct_social_square', 'shortcode_sbp_footer_social_square');

	function shortcode_get_name () {
		return get_option('sct-company-name');
	}

	function shortcode_get_address () {
		return get_option('sct-company-address');
	}

	function shortcode_get_city () {
		return get_option('sct-company-city');
	}

	function shortcode_get_state () {
		return get_option('sct-company-state');
	}

	function shortcode_get_zip () {
		return get_option('sct-company-zip');
	}

	function shortcode_get_email () {
		return get_option('sct-company-email');
	}

	function shortcode_get_phone () {
		return get_option('sct-company-phone');
	}

	function shortcode_get_phone_alt () {
		return get_option('sct-company-phone-alt');
	}

	function shortcode_get_fax () {
		return get_option('sct-company-fax');
	}

	function shortcode_get_locality () {
		return get_option('sct-company-locality');
	}

	$getter_shortcodes = [
		'name', 'address', 'city', 'state', 'zip', 'email', 'phone', 'phone_alt', 'fax', 'locality'
	];

	foreach ($getter_shortcodes as $attribute) {
		add_shortcode('sct_' . $attribute, 'shortcode_get_' . $attribute);
		add_shortcode('sbp_' . $attribute, 'shortcode_get_' . $attribute);
	}